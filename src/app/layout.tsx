// app/layout.tsx npx create-next-app@latest my-onboarding-app --typescript
import { Box } from '@chakra-ui/react'
import { Providers } from './providers'

export default function RootLayout({
  children,
}: {
  children: React.ReactNode,
}) {
  return (
    <html lang='en'>
      <body>
        <Providers><Box maxW="960px" mx="auto" my={50}>{children}</Box></Providers>
      </body>
    </html>
  )
}