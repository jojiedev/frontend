import { Box } from "@chakra-ui/react";
import CustomerPage from "./customer/page";

export default function Home() {
  return (
    <Box p={4}>
      <CustomerPage />
    </Box>
  );
}
