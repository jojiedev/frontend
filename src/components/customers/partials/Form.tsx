'use client'
import React from "react";
import {
  FormControl,
  FormLabel,
  Input,
  FormErrorMessage,
  Button,
  Box,
  Text,
  useToast
} from "@chakra-ui/react";
import { useForm } from "react-hook-form";
import { yupResolver } from '@hookform/resolvers/yup';
import * as yup from "yup";
import { useRouter } from 'next/navigation';

export interface IFormInput {
  firstName: string;
  lastName: string;
  email: string;
  phoneNumber: string;
  postcode: string;
}

const schema = yup.object({
  firstName: yup.string().max(100).required("First name is required"),
  lastName: yup.string().max(100).required("Last name is required"),
  email: yup.string().email("Invalid email").required("Email is required"),
  phoneNumber: yup
  .string()
  .matches(
    /^(?:0[2-478])[0-9]{8}$/,
    'Phone number must be a valid Australian phone number without country code'
  )
  .required('Phone number is required'),
  postcode: yup
  .string()
  .matches(
    /^[0-9]{4}$/,
    'Postcode must be a valid 4-digit Australian postcode'
  )
  .required('Postcode is required'),
}).required();

interface FormProps {
    defaultValues?: IFormInput;
    onSubmit: (data: IFormInput) => void;
    isSuccessful?: boolean;
  }

const Form: React.FC<FormProps> = ({ defaultValues, onSubmit, isSuccessful }) => {
    const toast = useToast();
    const [isSubmitting, setIsSubmitting] = React.useState(false);
    const { register, handleSubmit, formState: { errors }, reset } = useForm<IFormInput>({
    resolver: yupResolver(schema)
  });

  const handleFormSubmit = async (data: IFormInput) => {
    setIsSubmitting(true);
    try {
      await onSubmit(data);

      if (!isSuccessful) {
        toast({
            title: 'Error',
            description: 'An error occurred or Email already exists!',
            status: 'error',
            duration: 5000,
            isClosable: true,
        });
        return;
      }
      toast({
        title: 'Success',
        description: defaultValues ? 'User updated successfully.' : 'User created successfully.',
        status: 'success',
        duration: 5000,
        isClosable: true,
      });
      reset();
    } catch (error) {
      toast({
        title: 'Error',
        description: 'An error occurred or Email already exists!',
        status: 'error',
        duration: 5000,
        isClosable: true,
      });
    } finally {
      setIsSubmitting(false);
    }
  };

  return (
    <Box p={4}>
        <Text fontSize="xl" mb={4}>Customer Form</Text>
      <form onSubmit={handleSubmit(handleFormSubmit)}>
        <FormControl isInvalid={!!errors.firstName}>
          <FormLabel htmlFor="firstName">First Name</FormLabel>
          <Input id="firstName" {...register("firstName")} />
          <FormErrorMessage>{errors.firstName?.message}</FormErrorMessage>
        </FormControl>

        <FormControl isInvalid={!!errors.lastName}>
          <FormLabel htmlFor="lastName">Last Name</FormLabel>
          <Input id="lastName" {...register("lastName")} />
          <FormErrorMessage>{errors.lastName?.message}</FormErrorMessage>
        </FormControl>

        <FormControl isInvalid={!!errors.email}>
          <FormLabel htmlFor="email">Email</FormLabel>
          <Input id="email" type="email" {...register("email")} />
          <FormErrorMessage>{errors.email?.message}</FormErrorMessage>
        </FormControl>

        <FormControl isInvalid={!!errors.phoneNumber}>
          <FormLabel htmlFor="phoneNumber">Phone Number</FormLabel>
          <Input id="phoneNumber" {...register("phoneNumber")} />
          <FormErrorMessage>{errors.phoneNumber?.message}</FormErrorMessage>
        </FormControl>

        <FormControl isInvalid={!!errors.postcode}>
          <FormLabel htmlFor="postcode">Postcode</FormLabel>
          <Input id="postcode" {...register("postcode")} />
          <FormErrorMessage>{errors.postcode?.message}</FormErrorMessage>
        </FormControl>
        <Button
            mt={4}
            colorScheme="blue"
            type="submit"
            isLoading={isSubmitting}
            loadingText={defaultValues ? 'Updating...' : 'Creating...'}
        >
            {defaultValues ? 'Update' : 'Create'}
        </Button>
      </form>
    </Box>
  );
};

export default Form;
