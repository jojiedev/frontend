import React from 'react';
import { useQuery } from 'react-query';
import { Box, List, ListItem, Text, VStack, Heading } from '@chakra-ui/react';
import { IFormInput } from './partials/Form';
import { fetchUsers } from '../../services/users';

interface IUser {
    id: number;
    firstName: string;
    lastName: string;
    email: string;
    phoneNumber: string;
    postcode: string;
}

const UserList: React.FC = () => {
    const { data: users, error, isLoading } = useQuery('users', fetchUsers);

    if (isLoading) return <Text>Loading users...</Text>;
    if (error instanceof Error) return <Text>Error: {error.message}</Text>;
  
    return (
      <VStack spacing={4} align="stretch">
        <Heading as="h3" size="lg">User List</Heading>
        <Box borderWidth="1px" borderRadius="lg" overflow="hidden" p={4}>
          <List spacing={3}>
            {users?.map((user: IUser) => (
              <ListItem key={user.id}>
                {user.firstName} {user.lastName} - {user.email}
              </ListItem>
            ))}
          </List>
        </Box>
      </VStack>
    );
};

export default UserList;