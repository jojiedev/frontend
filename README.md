# Frontend Project

This project contains the frontend code for Carconnect Coding Challenge.

## Setup Instructions

Follow these steps to set up and run the frontend project:

1. Clone the repository:
    git clone git@gitlab.com:jojiedev/frontend.git

2. Navigate to the project directory:
    cd frontend

3. Install dependencies:
    npm install

4. Start the development server:
    npm run dev


5. Open your web browser and navigate to [http://localhost:3001](http://localhost:3001) to view the application.

## Show List Endpoint

To view the list, navigate to [http://localhost:3001/list](http://localhost:3001/list).

## Sample Payload

Use the following sample payload to test the API:

```json
{
"firstName": "Jame",
"lastName": "Tan",
"email": "tan.doe@example.com",
"phoneNumber": "0412345678",
"postcode": "12345"
}
