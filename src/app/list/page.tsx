'use client'
import React from "react";

import List from "../../components/customers/List";

const listPage: React.FC = () => {

  return (
    <>
      <List />
    </>
  );
};

export default listPage;
