import { IFormInput } from '../components/customers/partials/Form';

const BASE_URL = 'http://localhost:3000/api/v1';

export const createUser = async (userData: IFormInput) => {
  const response = await fetch(`${BASE_URL}/users`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(userData),
  });

  if (!response.ok) {
    throw new Error('Network response was not ok');
  }

  return response.json();
};

export const fetchUsers = async () => {
  const response = await fetch(`${BASE_URL}/users`);

  if (!response.ok) {
    throw new Error('Network response was not ok');
  }

  return response.json();
};

