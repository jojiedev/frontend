import React from 'react';
import Form from './partials/Form';
import { IFormInput } from './partials/Form';
import { useMutation } from 'react-query';
import { createUser } from '../../services/users';

const CreatePage: React.FC = () => {
    const [isSuccessful, setIsSuccessful] = React.useState(false);

    const createMutation = useMutation(createUser, {
        onSuccess: (data) => {},
    });

    const onSubmit = async (data: IFormInput) => {
        console.log('Form data:', data);

        const responseData = await createMutation.mutateAsync(data);
        if (responseData) {
            setIsSuccessful(true);
        }
      };

  return <Form onSubmit={onSubmit} isSuccessful={isSuccessful} />;
};

export default CreatePage;