'use client'
import React from "react";

import CreatePage from "../../components/customers/Create";

const CustomerPage: React.FC = () => {

  return (
    <>
      <CreatePage />
    </>
  );
};

export default CustomerPage;
